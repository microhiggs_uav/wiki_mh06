# Airframe

The MH06 quadcopter airframe features a modular design with vehicle body, arm and quick-mount landing gears three parts. This mechanical design makes it easy to transport,, store, and prepare for flight.

The MH06 quadcopter airframe design gives it an IP67 Ingress Protection, in accordance with the global IEC 60529 standard. 

## Aircraft

| Specifications               | Parameters  |
| ---------------------------- | ----------- |
| Diagonal Wheelbase           | 620mm       |
| Height                       | 365mm       |
| Weight(Battery included)     | 3.0 - 3.2kg |
| Max Payload                  | 1.0 - 1.2kg |
| Max Takeoff Weight           | 4.0 - 4.5kg |
| Max Flight Time with payload | >= 15min    |
| Waterproof                   | IP67        |

## Vehicle body

The vechicle body mainly consists of a upper shell and alower shell both using PC-ABS material. It serves two main functions - placing electronic parts and offering buoyancy when landing on the sea.

![](./image/airframe/1.png)


![](./image/airframe/2.png)



## Vehicle arm

The vehicle arm integrating ESC/motor sytem is modularly designed to bear the vehicle body.

![](./image/airframe/3.png)


![](./image/airframe/4.png)



## Landing gear

The landing gear is designed based on the helicopter's one to release vibration when landing.

![](./image/airframe/5.png)