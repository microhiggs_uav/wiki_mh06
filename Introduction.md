# Introduction

MH06 is an advanced technically fishing-industry-facing quadcopter developed by MicroHiggs Ltd. It consists of airframe, power system, flight control autopilot, RemoteControl(RC), GroundControlStation(GCS) APP and payload(typically camera and/or finishing sonar).



![](./image/introduction/1.jpg)



- [Airframe](./Airframe.md)
- [Power System](./Power%20System.md)
- [Autopilot User Guide](./Autopilot%20User%20Guide.md)
- [Autopilot Develop Guide](./Autopilot%20Develop%20Guide.md)
- [Parameter Configuration](./Parameter%20Reference%20and%20Configuration.md)
- [Remote Control Manual](./Remote%20Control%20Manual.md)
- [Ground Control Station](./Ground%20Control%20Station.md)
- [Payload](./Payload.md)

## Key features:

- IP67 waterproof quadcopter
- Able to land on the sea to perform tasks
- Detecting fish mode brings the capability to automatcally plan a mission and finish detecting finshes in the specified sea zone
- 3-in-1 communication link enables real-time vedioes and sonar-scan acoustic images
- 3 flight modes satisfy operating demands for almost all scenarios