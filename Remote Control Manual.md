# Remote Control

The remote control system is a highly integrated data communication system which has **RC, datalink and videolink** built inside. Equipped with state-of-the-art long-distance radio communication technology, the Remote control system supports automatic channel switching between 2.4 GHz and 2.483GHz, reducing the influence of environmental interference on drone operation and image quality. This also ensures reliable long-range transmission at distances of up to 10 km.

It consistes of a hand-held transmitter and a vehicle-end receiver, which of them communicate via 2.4G WIFI signal with each other.



## Technical specification

### Transmitter

| Specifications            | Parameters       |
| ------------------------- | ---------------- |
| work voltage              | 4.2V             |
| operating frequency       | 2.400 - 2.483GHz |
| power (EIRP)              | 20DB@CE/23DB@FCC |
| channels available        | 16               |
| work time                 | 6 - 12hours      |
| max transmission distance | 10km             |
| built-in battery capacity | 20000mAH         |
| dimensions                | 272x183x94mm     |
| weight(battery included)  | 1034g            |
| charger interface         | type-C           |

### Receiver

| Specifications | Parameters       |
| -------------- | ---------------- |
| work voltage   | 7.2 - 72V        |
| power (EIRP)   | 20DB@CE/23DB@FCC |
| dimensions     | 76x59x11mm       |
| weight         | 90g              |

### Camera

| Specifications | Parameters        |
| -------------- | ----------------- |
| work voltage   | 14 - 72V          |
| work current   | 14mA              |
| dimensions     | 102.1x42.6x36.5mm |
| weight         | 55g               |



## Hand-held transmitter 

The hand-held transmitter has a built-in 5.5-inch 1080p screen, providing an ultra-bright display to keep your live feed easily viewable, even in direct sunlight. Running a  customized Android system, it is designed to equiped with the [Ground Control Station APP](https://bitbucket.org/microhiggs_uav/fcu_168/wiki/Ground%20Control%20Station).

![](./image/remotecontrol/1.PNG)

The hand-held transmitter comes with diversified interface for expanded options. It supports PPM, WAN, type-C, OTG and TF, that meets users' diverse data service needs.

![](./image/remotecontrol/2.PNG)

The hand-held transmitter supports up to 16 channels and they are managed by H16 Assistant app in the android system.

![](./image/remotecontrol/3.PNG)

![](./image/remotecontrol/4.PNG)

Every channel can be configured for different operating aim. Conventially, MH06 uses CH1-CH7 for rpy/throttle control and mode switching, and other channels are reserved. The recommended configure is detaled belows:

| CH   | button | reverse | note                                                  |
| ---- | ------ | ------- | ----------------------------------------------------- |
| 1    | X2     |         | yaw                                                   |
| 2    | Y2     | yes     | throttle                                              |
| 3    | Y1     |         | pitch                                                 |
| 4    | X1     |         | roll                                                  |
| 5    | SW1    | yes     | upper - manual<br>middle - gps<br>lower - mission |
| 6    | SW4    | yes     | lower - althold                                       |
| 7    | SW3    | yes     | lower - rtl                                           |
| 8-16 |        |         | reserved                                              |

In addition, the transmitter provides a WIFI hotspot, via which ALT_GCS can be connect to the autopilot. The operating method can be found in Gournd Control Station page.

### Operating Manual

All romote parameter configuration can be finished by `Channel Configureation` in  `Advance Parameter`.

**Note** the default password for `Advance Parameter` is 999.

 ![](./image/remotecontrol/5.PNG)

 ![](./image/remotecontrol/6.PNG)

 ![](./image/remotecontrol/7.PNG)

The Pop-up window shows like below. Configure channels as before.

![](./image/remotecontrol/8.PNG)

All needed configuration finish after save the parameters by clicking `save` button.

The well-configureed transmitter will function as belows.

![](./image/remotecontrol/9.PNG)



## Vehicle-end receiver

As a 3-in-1 module, receiver communicates with flight control board via UART-port datalink and SBUS RC. Besides, it also supports MIPI for camera and HDMI for video transmition.

![](./image/remotecontrol/10.PNG)


![](./image/remotecontrol/11.jpg)

