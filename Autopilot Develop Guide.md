# Autopilot Developer Guide

Autopilot includes hardware board and hardware-based flight controller software.

## Hardware Board

### Overall Archtecture

The hardware board features power processor with an STM32F427 as master plus an STM32F100 as coprocessor. Such design enable it rich expansion.

![](./image/autopilot_develop_guide/1.jpg)


The following figures illustrate the board port defination.

![](./image/autopilot_develop_guide/2.jpg)



![](./image/autopilot_develop_guide/3.jpg)

### Technical Specifications

|                                   | **Hardware parameters**                |
| :-------------------------------- | :------------------------------------- |
| **Processor**                     |                                        |
| Main processor Processor          | STM32F427                              |
| Coprocessor Failsafe co-processor | STM32F100                              |
| **Sensor**                        |                                        |
| IMU                               | MPU9250                                |
| Magnemetor                        | HMC5883                                |
| Barometer                         | MS5611                                 |
| **Interface**                     |                                        |
| Mavlink UART                      | 2（With hardware flow control）        |
| GPS UART                          | 2                                      |
| DEBUG UART                        | 1                                      |
| Remote signal input protocol      | PPM/SBUS/DSM/DSM2                      |
| RSSI input                        | PWM or 3.3 analog voltage              |
| I2C                               | 2                                      |
| CAN Standard bus                  | 1                                      |
| ADC input                         | 3.3V X1 , 6.6V X1                      |
| PWM output                        | Standard 8 PWM IO + 6 Programmable IOs |

## Flight Controller Software

This part provides the overall development process and tools for autopilot software developers.

- download the source code
- setup developing environment and then build the source code
- upload firmware
- interface with firmware using NSH
- diagnose problems using log tool
- HITL simulator(Hoftware in the Loop)

### Download the Source Code

The source code name FCU168 is stored on Bitbucket in the [FCU_168](https://bitbucket.org/microhiggs_uav/fcu_168/src/master/) repository. To get the *very latest* version onto your computer, enter the following command into a terminal:

```
git clone https://qiaozj@bitbucket.org/microhiggs_uav/fcu_168.git 
```

> Git Bash is recommended as the termianl.

### Build the Source Code

One needs to setup the developing environment wihich can be supported on Windows and Linux before builing FCU168 firmware with make.

#### On Windows

1. Install [PX4 Console](Z:\飞控软件组\Tools)
2. Build

To build the source code, you would use the following command in PX4 Console:

```
cd fcu_168

make px4fmu-v2_default
```

A successful run will end with this output:

```
%% Generating ./fcu_168/Build/px4fmu-v2_default.build/firmware.px4
make[1]: Leaving directory `./fcu_168/Build/px4fmu-v2_default.build'
%% Copying ./fcu_168/Images/px4fmu-v2_default.px4
```

#### On Linux/Ubuntu

1. unzip [GCC4.7.4 compiler](Z:\飞控软件组\Tools\168 linux toolchain) to  /home/$USER/Packages
2. make the user a member of the group "dialout"

```
$sudo usermod -a -G dialout $USER  
```

> Note "log out" to activate it!!! Cannot upload firmeare to Pixhawk board without it.

3. set the compiler path (change the path accordingly)

```
$export PATH=/home/$USER/Packages/gcc-arm-none-eabi-4_7-2014q2/bin:$PATH
```

4. Make sure the tool chain is working:

```
$ arm-none-eabi-gcc --version
```

It will end with follows if he compiler works well.

`arm-none-eabi-gcc (GNU Tools for ARM Embedded Processors) 4.7.4 20140401 (release) [ARM/embedded-4_7-branch revision 209195]`
`Copyright (C) 2012 Free Software Foundation, Inc.`
`This is free software; see the source for copying conditions.  There is NO`
`warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.`

5. Install the necessary pkgs

```
sudo apt-get update

sudo apt-get install git zip qtcreator cmake build-essential genromfs ninja-build exiftool vim-common -y

sudo apt-get install libc6:i386 libgcc1:i386 libstdc++5:i386 libstdc++6:i386

sudo apt-get install python-pip python3-pip

pip install pyserial -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com

sudo apt-get remove modemmanager 

sudo apt-get install minicom
```


6. change some tool file permissions of the px4 firmware:

```
cd ~/fcu_168
chmod a+x NuttX/nuttx/*.sh
chmod a+x NuttX/nuttx/tools/*.sh
chmod a+x ~/Tools/*.sh
chmod a+x ~/Tools/*.py
```

7. Make 

```
make archives
make px4fmu-v2_default
```

A successful run will end with this output:

```
%% Generating /home/wxj/Desktop/master_linux/fcu_168/Build/px4fmu-v2_default.build/firmware.px4
make[1]: Leaving directory '/home/wxj/Desktop/master_linux/fcu_168/Build/px4fmu-v2_default.build'
%% Copying /home/wxj/Desktop/master_linux/fcu_168/Images/px4fmu-v2_default.px4
```

> **Note** Errors will happen for compiled source code in Windows to make in Linux eniveronment. So you have to download speciifically code for compiling in Linux environment.

### Upload Firmware

Append `upload` to the make commands to upload the compiled binary to the autopilot hardware via USB. For example

```sh
make px4_fmu-v2_default upload
```

A successful run will end with this output:

```sh
Erase  : [====================] 100.0%
Program: [====================] 100.0%
Verify : [====================] 100.0%
Rebooting.
```

### Interface with firmware using NSH

The firmware runs the NuttX real-time operating system which includes the NuttX Shell terminal “NSH”. This allows running some Unix style commands.

NSH is very useful for diagnosing low level issues. Some of the things you can do with it include:

- Display performance counters with the `perf` command
- Display px4io status information
- Diagnose microSD errors
- Diagnose sensor failures
- Assist in debugging new drivers

The running firmware can be debugged by connecting board and a serial port tool running on desktop PC via a USB convert to serial port cable.

A successful comminication would show follows:

```
NuttShell (NSH)
nsh> 
nsh> 
nsh> 
```

You can operate every thread by entering supported commands as in linux. For example, magnetometer sensor status will be got by following commads:

```
nsh> hmc5883 info
hmc5883: driver not running
```

### Diagnose problems using log tool

1. Download log tool. We use a  matlab code as the log tool.

```
git clone https://qiaozj@bitbucket.org/microhiggs_uav/mh06_log.git
```

2. Download log file. Log files are recorded to the SD card so they must be downloaded from the autopilot after a flight. Conventially we copy the newest log files with a SD card reader plugging in the PC.

![](D:/Wiki/wiki_1st/wiki/image/autopilot_user_guide/01.png)

3. Run the Matlab code to analyse the log information. 

Open the main program named **FCU_log_plot_BL168.m**, change the filename path to copied log file's direction, and run it .

![](D:/Wiki/wiki_1st/wiki/image/autopilot_user_guide/02.png)

4. A successful run will display data information graphs.

![](./image/autopilot_user_guide/03.png)

### HITL simulator

The HITL (hardware in the loop) simulator allows you to run flight controller code on the autopilot without any peripheral hardware, giving you a simulation environment on the ALT_GCS that allows you to test the behaviour of the code without hardware.

Typically, we use HITL in auto mode to test the behaviour for mission submode and RTL submode on Windows.

![](./image/autopilot_user_guide/06.jpg)

#### Configure HITL

To enter HITL simulator, you need to configure the /etc/rc.txt file in the TF card by:

- changing `mavlink start -d /dev/ttyS1 -b 115200` to `mavlink start -d /dev/ttyS2 -b 115200`
- adding `px4_simulation start`

The well-configured rc.txt comes shown belows.

![](./image/autopilot_user_guide/07.png)



> It is recommended to use the [rc.txt](./file/hitl/etc) and [parameter.txt](./file/hitl) to replace the old ones, which are specifically configured for SITL.

#### Build connection between autopilot and ALT_GCS

#### Build connection between the radio transmitter and autopilot

#### Create a mission on ALT_GCS and upload to the autopilot

#### Switch to auto mode through radio and click start cruise flight button on ALT_GCS

#### HILT will being running once the steps are finished

In general, we open the `ShowMessages window` and `data window` to get the quadcopter's state information when simulating in HILT.

![](./image/autopilot_user_guide/08.png)



![](./image/autopilot_user_guide/09.png)

> You should arm the autopilot in manual mode before switching to auto mode.

> The information regarding operation on ALT_GCS can be found on Ground Control Station page.

