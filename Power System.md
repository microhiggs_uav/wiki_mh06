# Power System

MH06's power system, constituted of a Lipo battery, ESCs, motors and propellers, is purposed-built to improve the quadcopter's dynamic response while maintaining maximum flight time.![](./image/power_system/power_system.jpg)


## Technical specification

| Item                     | Specification            |
| ------------------------ | ------------------------ |
| Motor                    | KV420                    |
| ESC                      | T-motor Air 40A          |
| Propeller                | 15.2 x 5                 |
| Battery                  | LiPo 6S@7000mAh          |
| Maximum thrust per motor | 2.254kg@22.2V 2.50kg@25V |