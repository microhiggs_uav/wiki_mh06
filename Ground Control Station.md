# Ground Control Station

Ground Control Station includes PC version named ALT_GCS and mobile version, which of them both integrate following functions: 

- Load the firmware (the software) into the autopilot board (i.e. Pixhawk series) that controls your vehicle.
- Calibrate sensors for prflight check
- Configure parameters for optimum performance.
- Plan, save and load autonomous missions into you autopilot with simple point-and-click way-point
- other commonly-used features

They can work simultaneously as well as individually.

## PC Version

### Install software

- Download the [ALT_GCS_Exe](Z:\飞控软件组\Tools)
- Double click on the `keygen` file and `git key to this pc`, and then copy the generated key to sfkey in the `advsettings` file

![](./image/groundcontrolstation/01.png)

![](./image/groundcontrolstation/02.png)

- Double click on the `qgroundcontrol` file to run it

![](./image/groundcontrolstation/03.png)

A successful run will show follow:

![](./image/groundcontrolstation/04.png)

### Connect ALT_GCS to FCU168

You can connect the PC and FCU168 via USB-to-serial-port cables (mainly for debugging or HITL simulator) or WIFI on receiver(mainly used when flying).

#### Via USB-to-serial-port cables

On ALT_GCS, the connection and data rate are set up. Once you’ve attached the USB, Windows will automatically assign your autopilot a COM port number, and that will show in the drop-down menu. Seclect the corresponding port and set up appropriate data rate(typically the USB connection data rate is 115200).

![](./image/groundcontrolstation/05.png)

![](./image/groundcontrolstation/06.png)


A successful run will end with green `signal 100%` icon and `motor locked` icon:

![](./image/groundcontrolstation/07.png)

#### Via WIFI on transmitter

Connect to the hotspot, whose name generally start with "AP".

Click **Menue**|**Quickly Connect**, and the icons `signal 100%` and `motor locked` will turn green once successful connection builds.

![](./image/groundcontrolstation/29.png)

## ALT_GCS Operating Manual ##

### Upload the firmware into the FCU168 autopilot board ###

Under **Menu | Tools**, select **Firmware Upgrade**. In the pop-up window, click `Select File` with ticking `Advanced`, and then choose the firmware generated in `~\fcu_168\Build\px4fmu-v2_default.build\firmware.px4`.

![](./image/groundcontrolstation/08.png)

![](./image/groundcontrolstation/09.png)

A successful upload will end this information "Upload succeeded.". If "Upload failed" is informed, you may unplug and replug the cable.

![](./image/groundcontrolstation/10.png)

> The default firmware named firmware.px4 can be downloaded [here](./file).
>

### Calibrate sensors ###

#### Calibrate Gyro ####

Under **Menu | Tools**, select **Sensors Calibration**. 

![](./image/groundcontrolstation/11.png)

Click **Calibrate Gyro** to start the gyro calibration. 

![](./image/groundcontrolstation/12.png)

> **Note keep the vehicle still until calibration finished.**

The icon will turn green once calibration succeeds,.

![](./image/groundcontrolstation/13.png)

#### Calibrate Accelemeter ####

Under **Menu | Tools**, select **Sensors Calibration**. 

Click **Calibrate Accel** to start the calibration. ALT_GCS will prompt you to place the vehicle each calibration position.

![](./image/groundcontrolstation/14.jpg)

> **Note It is important that the vehicle is kept still immediately after pressing the key for each step.**

When you’ve completed the calibration process, ALT_GCS will display “Accel Calibration Successful!” and the icon will green.



#### Calibrate Magnetometer ####

Under **Menu | Tools**, select **Magnetometer Calibration**. 

Hold the vehicle in the air and rotate it so that each side.

When you’ve completed the calibration process, ALT_GCS will display “Magnetometer Calibration Successful!” and the icon will green.

> **It is necessary to recalibrate the compass when the vehicle is flown at a new location.**



### Configure parameters ###

All vehicle parameters can be managed in **`Parameter Setting`** in the right-side menu.

You can download the latest parameter setting by clicking **`Read ROM`** and then **`Download`**. Actually the latest parameter setting will be refreshed automatically once connection is built. In the same way, newly tuned parameter can be uploaded by selecting **`Send`** and **`Write ROM`**.

![](./image/groundcontrolstation/15.png)

Parameter setting can be saved in file by clicking **`Save in file`**, and uploaded by **`Upload in file`**.

Conventially, parameters in **`UAV`** and **`MC`** need be tuned according to vehicles.

The default parameter named `parameters_MH06-3.txt` can be downloaded [here](./file).


> **Note, Before operating the vehicle, you must setup: senors calibration and parameter configuration.**

### Autonomous mission

There are, in general, 4 types of waypoints for auto mission mode: general waypoint, RH(ReturnHome) waypoint, geofence waypoint and no-fly-zone waypoint, and are illustrated in different colors. They can be created by double clicking on the map after right click and selecting the needed waypoint type in the pop-up menue, and the mission will be generated automatically. After that the created mission could be uploaded to the autopilot via datalink by clicking `upload` button and the uploaded mission will function once you click `start cruise` in auto mode.

![](./image/groundcontrolstation/16.png)

#### General waypoint

![](./image/groundcontrolstation/17.png)

![](./image/groundcontrolstation/18.png)

![](./image/groundcontrolstation/19.png)

#### RH waypoint

![](./image/groundcontrolstation/20.png)

![](./image/groundcontrolstation/21.png)

![](./image/groundcontrolstation/22.png)

#### Geofence waypoint

![](./image/groundcontrolstation/23.png)

![](./image/groundcontrolstation/24.png)

![](./image/groundcontrolstation/25.png)

#### No-fly-zone waypoint

![](./image/groundcontrolstation/26.png)

![](./image/groundcontrolstation/27.png)

![](./image/groundcontrolstation/28.png)

### Other commonly-used features

#### Information column

The state information column indicates how the vehicle is running. It is necessary to make sure them in appropriate status before arming the quadcopter.

|           | red          | green     | preflight check               | notes                |
| --------- | ------------ | --------- | ----------------------------- | -------------------- |
| battery   |              |           | >=80%                         |                      |
| satelites |              |           | >=8                           | GPS satelites number |
| signal    | disconnected | connected | connected if auto mode needed | datalink signal      |
| RC signal | disconnected | connected | connected                     |                      |

![](./image/groundcontrolstation/30.png)

#### Tool box

The **Tool box|data instrument** indicates the vehicle's motion state. 

![](./image/groundcontrolstation/31.png)

The `ShowMessages` window comes with autopilot's output showing how the autopilot is running.

![](./image/groundcontrolstation/32.png)