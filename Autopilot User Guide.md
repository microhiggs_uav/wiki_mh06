# Autopilot User Guide

## Autopilot and the peripheral

Autopilot along with the peripheral as shown below consitutes the whole electrical sytem for MH06.

![](./image/autopilot_user_guide/04.jpg)

## Flight modes

The autopilot offers 4 main built-in flight modes and 1 offboard mode for simulation:

- Manual mode
- Althold mode
- GPS mode
- Auto mode
- Offboard mode

The first 3 modes(i.e. manual, althold and GPS mode) can be accessible via a transmitter switch directly, whose defination is included in the Remote Control Manual page, while auto mode needs to cooperate with ALT_GCS to trigger the 2 submodes that will come below. Information on operation in auto mode can be found on the Ground Control Station page.

### Manual mode

Manual mode allows you to fly your vehicle manually, but self-levels the roll and pitch axis. In this mode, it features:

- pilot’s roll and pitch input control the lean angle of the copter. When the pilot releases the roll and pitch sticks the vehicle automatically levels itself.
- Pilot’s yaw input controls the rate of change of the heading. When the pilot releases the yaw stick the vehicle will maintain its current heading.
- Pilot’s throttle input controls the average motor speed meaning that constant adjustment of the throttle is required to maintain altitude.

### Althold mode

In altitude hold mode, autopilot maintains a consistent altitude while allowing roll, pitch, and yaw to be controlled normally.  Actually, compared to manual mode, the throttle is automatically controlled to maintain the current altitude to release pilot's operation and roll, pitch and yaw operate the same as in manual mode meaning that the pilot directly controls the roll and pitch lean angles and the heading.

### GPS mode

GPS mode automatically attempts to maintain the current location, heading and altitude. The pilot may fly the copter in Loiter mode as if it were in a more manual flight mode but when the sticks are released, the vehicle will slow to a stop and hold position.  

A good GPS lock is important in achieving good performance.

### Atuo mode

In auto mode, autopilot will perform navigation commands from pilot or ALT_GCS. As a main mode, it has 2 submode: mission mode and RTL mode.

#### Mission mode

In mission mode, the navigation commands is, in general, a pre-programmed waypoint mission. 

#### RTL mode

RTL mode (Return To Launch mode) navigates the quadcopter from its current position to hover above the home position. 

### Offboard mode

Offboard mode offers the capability to simulate the flight trajectory and is commonly used to vavify the code logics. As a develop tool, offboard mode will NOT be used by common user, and its information is detailed in Autopilot Develop Guide page.

## Advanced features

#### Geofence

A GeoFence is a virtual boundary within which a vehicle can travel. GeoFences can be used to prevent a vehicle flying out of range of the RC controller, or into unsafe or restricted airspace.

With the assist of ALT_GCS, users can specify a geofence to achieve safe flight. 

#### No-fly-zone

Compared to Geofence, no-fly zone define where a vechicle can NOT travel for safety.

> You can get details regarding geofence and no-fly zone from Ground Control Station page.

## Quick Start Guide

1. [upload firmware](./Ground Control Station.md)

2. [configure parameters on ALT_GCS](./Ground Control Station.md)

3. [calibrate sensors](./Ground Control Station.md)

4. turn on the RC

5. turn on the quadcopter by dirctly connecting the battery to autopilot

6. [preflight check](./Ground Control Station.md)

   * battery check

     make sure voltage is not less than 22.2V.

   * radio check 

     make sure connection with quadcopter has been achieved.

     make sure the flight mode switch is set to manual mode.

   * datalink check

     make sure connection with the autopilot via the transmitter's wifi has been built.

     create a mission and upload to the autopilot if a mission task is needed.

7. arm the quadcopter and fly

   Arm and disarm both are achieved by stick combination operation as illustrated belows for 3 seconds.

   Raise the throttle to take-off.

![](./image/autopilot_user_guide/11.jpg)