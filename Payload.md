# Payload

MH06 integrates a forward camera on the nose and a downward sonar under the body. Connected to receiver, they allow acquired date tranmit to ALT_GCS.

![](./image/payload/payload.jpg)

## Technical Specification

### Camera

| Specifications | Parameters        |
| -------------- | ----------------- |
| work voltage   | 14 - 72V          |
| work current   | 14mA              |
| dimensions     | 102.1x42.6x36.5mm |
| weight         | 55g               |

