# Parameter Reference

## Attitude estimation

| Name            | Description                                                  | Min - Max                  | Default | Units |
| :-------------- | ------------------------------------------------------------ | -------------------------- | ------- | ----- |
| ATT_ACC_COMP    | Acceleration compensation based on GPS velocity.             | 0 - Disable<br/> 1- Enable | 1       |       |
| ATT_BIAS_MAX    | Gyro bias limit                                              | 0.0 - 2.0                  | 0.05    |       |
| ATT_DUALGPS_DIR | NOT used                                                     |                            |         |       |
| ATT_DUALGPS_OFF | NOT used                                                     |                            |         |       |
| ATT_EXT_HDG_M   | External heading usage mode (from Motion capture/Vision)<br/> Set to 1 to use heading estimate from vision.<br/> Set to 2 to use heading from motion capture. | 0 - 2                      | 0       |       |
| ATT_MAG_DECL    | NOT used.<br>As the declination is looked up based on the GPS coordinates of the vehicle. |                            |         |       |
| ATT_MAG_DECL_A  | Automatic GPS based declination compensation                 | 0 - Disable<br/>1- Enable  | 1       |       |
| ATT_W_ACC       | Complimentary filter accelerometer weight                    | 0.0 - 1.0                  | 0.2     |       |
| ATT_W_EXT_HDG   | Complimentary filter external heading weight                 | 0.0 - 1.0                  | 0.1     |       |
| ATT_W_GYRO_BIAS | Complimentary filter gyroscope bias weight                   | 0.0 - 1.0                  | 0.1     |       |
| ATT_W_MAG       | Complimentary filter magnetometer weight                     | 0.0 - 1.0                  | 0.1     |       |

## Battery

| Name            | Description                                                  | Min - Max | Default | Units |
| --------------- | ------------------------------------------------------------ | --------- | ------- | ----- |
| BAT_CAPACITY    | Define battery capacity.                                     |           |         |       |
| BAT_C_SCALING   | Scaling factor for battery current sensor<br>FMUv1 standalone: 1/(10 / (47+10)) * (3.3 / 4095) = 0.00459340659 |           |         |       |
| BAT_LOW_LAND    |                                                              |           |         |       |
| BAT_LOW_RTL     |                                                              |           |         |       |
| BAT_N_CEILS     | Define numer of battery cells.                               |           |         |       |
| BAT_V_CHARGED   | Defines the voltage where a single cell of the battery is considered full. |           | 4.2     | V     |
| BAT_V_EMPTY     | Defines the voltage where a single cell of the battery is considered empty. |           | 3.4     | V     |
| BAT_V_I_DIFF    | Estimate battery by voltage and current, if their difference is larger than this threshold value consider the battery is bad performance. | 0.0 - 1.0 | 0.2     |       |
| BAT_V_LOAD_DROP | Voltage drop per cell on 100% load<br/> This implicitely defines the internal resistance to maximum current ratio and assumes linearity. |           | 0.07    |       |
| BAT_V_LOW       | Low battery percentage estimated by voltage                  | 0.0 - 1.0 | 0.2     |       |
| BAT_V_SCALE_IO  | Scaling factor for battery voltage sensor on PX4IO           |           | 10000   |       |
| BAT_V_SCALING   | Scaling factor for battery voltage sensor on FMU v2<br>for R70 = 133K, R71 = 10K --> scale = 1.8 * 143 / (4096*10) = 0.0063 |           | 0.0063  |       |

## Circuit breaker

Analog to real aviation circuit breakers these parameters allow to disable subsystems. They are not supported as standard operation procedure and are only provided for development purposes. To ensure they are not activated accidentally, the associated parameter needs to set to the key (magic).

| Name            | Description                                                  | Min - Max  | Default | Units |
| --------------- | ------------------------------------------------------------ | ---------- | ------- | ----- |
| CBRK_AIRSPD_CHK | Circuit breaker for airspeed sensor<br>Setting this parameter to 162128 will disable the check for an airspeed sensor. | 0 - 162128 | 0       |       |
| CBRK_IO_SAFETY  | Circuit breaker for IO safety<br>Setting this parameter to 894281 will disable IO safety. | 0 - 22027  | 0       |       |
| CBRK_NO_VISION  |                                                              |            |         |       |
| CBRK_RATE_CTRL  |                                                              |            |         |       |
| CBRK_SUPPLY_CHK | Circuit breaker for power supply check<br>Setting this parameter to 894281 will disable the power valid | 0 - 894281 | 0       |       |

## CHECK

| Name             | Description | Min - Max | Default | Units |
| ---------------- | ----------- | --------- | ------- | ----- |
| CHECK_CURRENT    |             |           |         |       |
| CHECK_MOTOR_DIFF |             |           |         |       |
| CHECK_ON_OFF     |             |           |         |       |

## Communication

| Name           | Description                                                  | Min - Max | Default | Units  |
| -------------- | ------------------------------------------------------------ | --------- | ------- | ------ |
| COM_DL_LOSS_EN | Datalink loss mode enabled.<br/>Set to 1 to enable actions triggered when the datalink is lost. | 0 - 1     | 0       |        |
| COM_DL_LOSS_T  | Datalink loss triggered time.                                | 0 - 65535 | 10      | second |
| COM_RC_LOSS_T  | RC loss triggered time.                                      | 0 - 65535 | 5       | second |

## EKF

parameters for attitude_est_ekf. NOT used.

Firmware

| Name         | Description      | Min - Max | Default | Units |
| ------------ | ---------------- | --------- | ------- | ----- |
| FIRMWARE_VER | firmware version |           | 4.3     |       |

## Fish

| Name        | Description | Min - Max                 | Default | Units |
| ----------- | ----------- | ------------------------- | ------- | ----- |
| FISH_DETECT |             | 0 - Disable<br/>1- Enable | 0       |       |

## FIX

| Name            | Description                                                  | Min - Max | Default | Units |
| --------------- | ------------------------------------------------------------ | --------- | ------- | ----- |
| FIX_AUTO_YAW    | 0 for heading always facing forward,<br>1 for heading planned to predefined heading<br>2 for manual control |           |         |       |
| FIX_POS_SHT_DIS | fixed time shutter distance                                  |           |         |       |
| FIX_POS_SHT_EN  | whether enable the fixed position shutter or not             |           |         |       |
| FIX_TIME_SHT_EN | for return home not land but hover, 1 for return home and land. |           |         |       |
| FIX_TIME_SHT_T  | fixed time shutter time                                      |           |         |       |

## FULL

| Name           | Description | Min - Max | Default | Units |
| -------------- | ----------- | --------- | ------- | ----- |
| FULL_MANUAL_EN |             |           | 1       |       |

## Gimble

NOT used.

| Name       | Description | Min - Max | Default | Units |
| ---------- | ----------- | --------- | ------- | ----- |
| GB_P_BIAS  |             |           |         |       |
| GB_P_RATIO |             |           |         |       |
| GB_R_BIAS  |             |           |         |       |
| GB_R_RATIO |             |           |         |       |

## Geofence

| Name       | Description                                                  | Min - Max                       | Default | Units |
| ---------- | ------------------------------------------------------------ | ------------------------------- | ------- | ----- |
| GF_ALTMODE | Select which altitude reference should be used               | 0 - WGS84<br>1 = AMSL           |         |       |
| GF_ALT_MAX | maximum alt                                                  |                                 | 200     |       |
| GF_ALT_MIN | minmum alt                                                   |                                 | -10     |       |
| GF_COUNT   | Geofence counter limit<br/> Set how many subsequent position measurements outside of the fence are needed before geofence violation is triggered | -1 - 10                         | 5       |       |
| GF_ON      | Enable geofence.<br/>Set to 1 to enable geofence.<br/> Defaults to 1 because geofence is only enabled when the geofence.txt file is present. |                                 | 1       |       |
| GF_SOURCE  | Select which position source should be used. Selecting GPS instead of global position makes sure that there is no dependence on the position estimator<br/> | 0 - global position<br> 1 - GPS | 0       |       |

## Gimbal

| Name       | Description | Min - Max | Default | Units |
| ---------- | ----------- | --------- | ------- | ----- |
| GIMBAL_CTL |             |           | 0       |       |

## INAV position estimation

| Name             | Description                                                  | Min - Max                 | Default  | Units  |
| ---------------- | ------------------------------------------------------------ | ------------------------- | -------- | ------ |
| INAV_BARO_BETA   | NOT used                                                     |                           | 0.6      |        |
| INAV_CAMERA_TILT | NOT used                                                     |                           |          |        |
| INAV_DELAY_GPS   | GPS delay compensation                                       |                           | 0.2      | second |
| INAV_DELAY_MEA   | GPS delay compensation                                       |                           | 0.2      | second |
| INAV_DIST_FILT   | NOT used                                                     |                           |          |        |
| INAV_DIST_V_TOL  | threshold                                                    |                           |          |        |
| INAV_FLOW_K      | Factor to convert raw optical flow (in pixels) to radians [rad/px]. | 0.0 - 1.0                 | 0.15     |        |
| INAV_FLOW_Q_MIN  | Minimal acceptable optical flow quality<br>0.0 - lowest quality, 1.0 - best quality. | 0.0 - 1.0                 | 0.5      |        |
| INAV_GPS_OFFX    | GPS antenna offsetX in body frame                            |                           | 0.0      |        |
| INAV_GPS_OFFY    | GPS antenna offsetY in body frame                            |                           | 0.0      |        |
| INAV_GPS_OFFZ    | GPS antenna offsetZ in body frame                            |                           | 0.0      |        |
| INAV_IN_AIR_THR  |                                                              |                           |          |        |
| INAV_LAND_DISP   |                                                              |                           |          |        |
| INAV_LAND_T      |                                                              |                           |          |        |
| INAV_LAND_THR    | Land detector throttle threshold<br/>Value should be lower than minimal hovering thrust. Half of it is good choice. | 0.0 - 1.0                 | 0.15     |        |
| INAV_LASER_ERR   |                                                              |                           |          |        |
| INAV_LASER_FILT  | Weight for sonar filter<br/>Sonar filter detects spikes on sonar measurements and used to detect new surface level. | 0.0 - 1.0                 | 0.05     |        |
| INAV_MAX_ACC_BIA | Accel bias threshold                                         |                           | 0.5      |        |
| INAV_MAX_CORR_ER |                                                              |                           | 3.0      |        |
| INAV_Q_ACC       | Q convariance matrix diag value                              |                           | 0.09     |        |
| INAV_Q_ACC_BIAS  | matrix diag value                                            |                           | 0.000001 |        |
| INAV_R_GPS_FLOW  | XY axis weight factor for GPS when optical flow available<br/>When optical flow data available, multiply GPS weights (for position and velocity) by this factor. | 0.0 1.0                   | 0.1      |        |
| INAV_R_XY_FLOW   | * XY axis weight for optical flow<br/>Weight (cutoff frequency) for optical flow (velocity) measurements. | 0.0 - 10.0                | 5.0      |        |
| INAV_R_XY_GPS_P  | XY axis weight for GPS position<br/>Weight (cutoff frequency) for GPS position measurements. | 0.0 - 10.0                | 1.0      |        |
| INAV_R_XY_GPS_V  | XY axis weight for GPS velocity<br/>Weight (cutoff frequency) for GPS velocity measurements. | 0.0 - 10.0                | 0.1      |        |
| INAV_R_XY_MEA_P  | XY axis weight for slam measurement position<br/>Weight (cutoff frequency) for slam measurements. | 0.0 - 10.0                | 0.01     |        |
| INAV_R_XY_MEA_V  | XY axis weight for slam measurement velocity<br/>Weight (cutoff frequency) for slam measurements. | 0.0 - 10.0                | 0.1      |        |
| INAV_R_XY_RES_V  | XY axis weight for resetting velocity<br/>When velocity sources lost slowly decrease estimated horizontal velocity with this weight. | 0.0 - 10.0                | 0.5      |        |
| INAV_R_XY_VIS_P  | XY axis weight for vision position<br/>Weight (cutoff frequency) for vision position measurements. | 0.0 - 10.0                | 5.0      |        |
| INAV_R_XY_VIS_V  | XY axis weight for vision velocity<br/>Weight (cutoff frequency) for vision velocity measurements. | 0.0 - 10.0                | 0.0      |        |
| INAV_R_Z_BARO    | Z axis weight for barometer<br/>Weight (cutoff frequency) for barometer altitude measurements. | 0.0 - 10.0                | 0.5      |        |
| INAV_R_Z_DIST    |                                                              |                           |          |        |
| INAV_R_Z_GPS_P   | Z axis weight for GPS position<br/>Weight (cutoff frequency) for GPS position measurements. |                           |          |        |
| INAV_R_Z_GPS_V   | Z axis weight for GPS velocity<br/>Weight (cutoff frequency) for GPS velocity measurements. |                           |          |        |
| INAV_R_Z_MEA_P   | Z axis weight for slam measurement<br/>Weight (cutoff frequency) for vision altitude measurements. vision altitude data is very noisy and should be used only as slow correction for baro offset. | 0.0 - 10.0                | 0.1      |        |
| INAV_R_Z_MEA_V   | Z axis weight for slam measurement<br/>Weight (cutoff frequency) for vision altitude velocity measurements. vision altitude data is very noisy and should be used only as slow correction for baro offset. | 0.0 - 10.0                | 1.0      |        |
| INAV_R_Z_VIS_P   | Z axis weight for vision<br/>Weight (cutoff frequency) for vision altitude measurements. vision altitude data is very noisy and should be used only as slow correction for baro offset. | 0.0 - 10.0                | 0.5      |        |
| INAV_TAO_BARO    |                                                              |                           |          |        |
| INAV_USE_BARO    | enable baro                                                  | 0 - Disable<br/>1- Enable |          |        |
| INAV_USE_DIST    |                                                              | 0 - Disable<br/>1- Enable |          |        |
| INAV_USE_NUCZ    | NOT used                                                     |                           |          |        |

## Inflight check

| Name             | Description                                           | Min - Max | Default | Units |
| ---------------- | ----------------------------------------------------- | --------- | ------- | ----- |
| INFCHK_MAIN_EN   | inflightcheck enabled.<br/> Set to 1 to enable it.    | 0 - 1     | 0       |       |
| INFCHK_SENFAILED | gyro redundancy enabled.<br/>Set to 1 to enable it.   | 0 - 1     | 0       |       |
| INFCHK_SENRD_EN  | sensor redundancy enabled.<br/>Set to 1 to enable it. | 0 - 1     | 1       |       |

## Launch

| Name          | Description                                                  | Min - Max | Default | Units  |
| ------------- | ------------------------------------------------------------ | --------- | ------- | ------ |
| LAUN_ALL_ON   | NOT used                                                     |           |         |        |
| LAUN_CAT_A    | Catapult accelerometer theshold.<br/>LAUN_CAT_A * LAUN_CAT_T serves as threshold to trigger launch detection. |           | 30.0    |        |
| LAUN_CAT_MDEL | Motor delay<br/>Delay between starting attitude control and powering up the throttle (giving throttle control to the controller)<br/>Before this timespan is up the throttle will be set to LAUN_THR_PRE, set to 0 to deactivate |           | 0.0     | second |
| LAUN_CAT_PMAX |                                                              |           |         |        |
| LAUN_CAT_T    | Catapult time theshold.<br/>LAUN_CAT_A * LAUN_CAT_T serves as threshold to trigger launch detection. |           | 0.05    | second |
| LAUN_THR_PRE  | Throttle setting while detecting launch.<br/>The throttle is set to this value while the system is waiting for the take-off. |           | 0.0     |        |

## Mavlink

| Name          | Description                                                  | Min - Max | Default | Units |
| ------------- | ------------------------------------------------------------ | --------- | ------- | ----- |
| MAV_COMP_ID   | component ID                                                 |           |         |       |
| MAV_SYS_ID    | system ID                                                    |           |         |       |
| MAV_TYPE      | mavlink_system.type                                          |           |         |       |
| MAV_USEHILGPS | NOT used<br>Accept GPS HIL messages (for example from an external motion capturing system to fake indoor gps) |           |         |       |

## Attitude control

| Name           | Description                                                  | Min - Max | Default | Units |
| -------------- | ------------------------------------------------------------ | --------- | ------- | ----- |
| MC_ACRO_P_MAX  | Max acro pitch rate                                          |           | 90.0    | deg/s |
| MC_ACRO_R_MAX  | Max acro roll rate                                           |           | 90.0    | deg/s |
| MC_ACRO_Y_MAX  | Max acro yaw rate                                            |           | 120.0   | deg/s |
| MC_MAN_P_MAX   | Max manual pitch rate                                        |           | 35.0    | deg/s |
| MC_MAN_R_MAX   | Max manual roll rate                                         |           | 35.0    | deg/s |
| MC_MAN_Y_MAX   | Max manual yaw rate                                          |           | 120.0   | deg/s |
| MC_PITCHRATE_D | Pitch rate D gain<br/>Pitch rate differential gain. Small values help reduce fast oscillations. If value is too big oscillations will appear again. | >=0.0     | 0.002   |       |
| MC_PITCHRATE_I | Pitch rate I gain<br/>Pitch rate integral gain. Can be set to compensate static thrust difference or gravity center offset. | >=0.0     | 0.0     |       |
| MC_PITCHRATE_P | Pitch rate P gain<br/>Pitch rate proportional gain, i.e. control output for angular speed error 1 rad/s. | >=0.0     | 0.1     |       |
| MC_PITCH_I     | Pitch I gain                                                 | >=0.0     | 0.0     |       |
| MC_PITCH_P     | Pitch P gain<br/>Pitch proportional gain, i.e. desired angular speed in rad/s for error 1 rad. | >=0.0     | 6.0     |       |
| MC_REJ_WIND_EN |                                                              |           |         |       |
| MC_ROLLRATE_D  | Roll rate D gain                                             | >=0.0     | 0.002   |       |
| MC_ROLLRATE_I  | Roll rate I gain                                             | >=0.0     | 0.0     |       |
| MC_ROLLRATE_P  | Roll rate P gain                                             | >=0.0     | 0.1     |       |
| MC_ROLL_I      | Roll I gain                                                  | >=0.0     | 0.0     |       |
| MC_ROLL_P      | Roll P gain                                                  | >=0.0     | 0.1     |       |
| MC_YAWRATE_D   | Yaw rate D gain<br/>Yaw rate differential gain. Small values help reduce fast oscillations. If value is too big oscillations will appear again. | >=0.0     | 0.0     |       |
| MC_YAWRATE_I   | Yaw rate I gain<br/>Yaw rate integral gain. Can be set to compensate static thrust difference or gravity center offset. | >=0.0     | 0.0     |       |
| MC_YAWRATE_MAX | Max yaw rate<br/>Limit for yaw rate, has effect for large rotations in autonomous mode, to avoid large control output and mixer saturation. | >=0.0     | 120.0   | deg/s |
| MC_YAWRATE_P   | Yaw rate P gain<br/>Yaw rate proportional gain, i.e. control output for angular speed error 1 rad/s. | >=0.0     | 0.3     |       |
| MC_YAW_FF      | Yaw feed forward<br/>Feed forward weight for manual yaw control. 0 will give slow responce and no overshot, 1 - fast responce and big overshot. | 0.0 - 1.0 | 0.5     |       |
| MC_YAW_I       | Yaw I gain                                                   | >=0.0     | 0.0     |       |
| MC_YAW_P       | Yaw P gain<br/>Yaw proportional gain, i.e. desired angular speed in rad/s for error 1 rad. | >=0.0     | 2.0     |       |

## Mission

| Name            | Description                                                  | Min - Max | Default | Units |
| --------------- | ------------------------------------------------------------ | --------- | ------- | ----- |
| MIS_ALTMODE     | NOT used<br/>Altitude setpoint mode<br/>0: the system will follow a zero order hold altitude setpoint<br/>1: the system will follow a first order hold altitude setpoint<br/>values follow the definition in enum mission_altitude_mode | 0 - 1     | 0       |       |
| MIS_DIST_1WP    | NOT used<br/>Maximal horizontal distance from home to first waypoint<br/>Failsafe check to prevent running mission stored from previous flight at a new takeoff location.<br/>Set a value of zero or less to disable. The mission will not be started if the current waypoint is more distant than MIS_DIS_1WP from the current position. | 0 - 1000  | 500     |       |
| MIS_ONBOARD_EN  | NOT used<br/>Enable persistent onboard mission storage<br/>When enabled, missions that have been uploaded by the GCS are stored and reloaded after reboot persistently. | 0 - 1     | 1       |       |
| MIS_TAKEOFF_ALT | NOT used<br/>Take-off altitude<br/>Even if first waypoint has altitude less then MIS_TAKEOFF_ALT above home position, system will climb to MIS_TAKEOFF_ALT on takeoff, then go to waypoint. |           | 10.0    |       |

## Position controller

| Name            | Description                                                  | Min - Max  | Default | Units |
| --------------- | ------------------------------------------------------------ | ---------- | ------- | ----- |
| MPC_LAND_SPEED  | Landing descend velocity                                     | >=0.0      | 1.0     |       |
| MPC_MANUAL_JMAX | Maximum jerk for manual mode                                 | >=0.0      | 10.0    |       |
| MPC_MAN_MAX_INT | Maximum acc generated from integration item                  |            | 0.35    |       |
| MPC_THR_MAX     | Limit max allowed thrust.                                    | 0.0 - 1.0  | 1.0     |       |
| MPC_THR_MIN     | Minimum thrust for throttle channel<br/>Minimum vertical thrust. It's recommended to set it > 0 to avoid free fall with zero thrust. | 0.0 - 1.0  | 0.1     |       |
| MPC_TILTMAX_AIR | Maximum tilt during in air.                                  | 0.0 - 90.0 | 45.0    |       |
| MPC_TILTMAX_LND | Maximum tilt during landing<br/>Limits maximum tilt angle on landing. | 0.0 - 90.0 | 15.0    |       |
| MPC_USE_JYST    | move position setpoint with roll/pitch stick<br/>if rc sticks are in idle position and we allow joystick control from GCS, then take GCS control command otherwise use the RC joystick command | 0 - 1      | 0       |       |
| MPC_XY_FF       | NOT used<br/>Horizontal velocity feed forward<br/>Feed forward weight for position control in position control mode (POSCTRL). 0 will give slow responce and no overshot, 1 - fast responce and big overshot. | 0.0 - 1.0  | 0.5     |       |
| MPC_XY_I        | Integral gain for horizontal position error<br/>Non-zero value allows to resist wind. | >=0.0      | 0.0     |       |
| MPC_XY_OFF_MAX  | Limit the error between the reference and the measurement position. | >=0.0      | 5.0     |       |
| MPC_XY_P        | P gain for horizontal position error.                        | >=0.0      | 1.0     |       |
| MPC_XY_VEL_D    | Differential gain for horizontal velocity error.<br/>Small values help reduce fast oscillations. If value is too big oscillations will appear again. | >=0.0      | 0.01    |       |
| MPC_XY_VEL_I    | Integral gain for horizontal position error<br/>Non-zero value allows to resist wind. | >=0.0      | 0.02    |       |
| MPC_XY_VEL_MAX  | Maximum horizontal velocity in AUTO mode and endpoint for position stabilized mode (POSCTRL). | >=0.0      | 5.0     |       |
| MPC_XY_VEL_P    | P gain for horizontal velocity error.                        | >=0.0      | 0.1     |       |
| MPC_Z_CTL_MAX   | NOT used                                                     |            | 0.15    |       |
| MPC_Z_FF        | NOT used                                                     |            | 0.5     |       |
| MPC_Z_I         | Integral gain for horizontal altitude error.                 | >=0.0      | 0.0     |       |
| MPC_Z_MAX       | maximum flight height                                        |            | 100.0   |       |
| MPC_Z_OFF_MAX   |                                                              |            |         |       |
| MPC_Z_P         | P gain for altitude error.                                   | >=0.0      | 1.0     |       |
| MPC_Z_VEL_D     | Differential gain for vertical velocity error                | >=0.0      | 0.0     |       |
| MPC_Z_VEL_I     | Integral gain for vertical velocity error<br/>Non zero value allows hovering thrust estimation on stabilized or autonomous takeoff. | >=0.0      | 0.02    |       |
| MPC_Z_VEL_MAX   | Maximum vertical velocity<br/>Maximum vertical velocity in AUTO mode and endpoint for stabilized modes (ALTCTRL, POSCTRL). | >=0.0      | 5.0     | m/s   |
| MPC_Z_VEL_P     | P gain for altitude velocity error.                          | >=0.0      | 0.1     |       |

## Navigator

| Name            | Description                                                  | Min - Max | Default | Units |
| --------------- | ------------------------------------------------------------ | --------- | ------- | ----- |
| NAV_ACC_RAD     | NOT used<br/>Acceptance Radius.                              |           | 25.0    |       |
| NAV_ACC_XY_NOR  | NOT used                                                     |           | 2.0     |       |
| NAV_ACC_Z_NOR   | NOT used                                                     |           | 2.0     |       |
| NAV_DIRECT_RH   | enable directly return home                                  | 0 - 1     | 0       |       |
| NAV_LOITER_RAD  | NOT used, only availabel for FW                              |           |         |       |
| NAV_MAX_ACC_XY  | Maximum horizontal accel                                     |           | 0.5     | m/s/s |
| NAV_MAX_ACC_YAW |                                                              |           |         |       |
| NAV_MAX_ACC_Z   | Maximum vertical accel                                       |           | 0.5     |       |
| NAV_MAX_JERK    | Maximum horizontal jerk                                      |           | 0.6     |       |
| NAV_RHC_HEIGHT  | flight height for returning home                             |           | 16.0    | m     |
| NAV_RH_LAND_EN  | 0 for return home not land but hover<br/>1 for return home and land. | 0 - 1     | 1       |       |
| NAV_RH_WP_TYPE  | return home waypoint type<br/>1 for following the visited waypoints and 0 for following predefined waypoints | 0 - 1     | 0       |       |
| NAV_RMAX        | JLT parameter                                                |           | 0.6     |       |
| NAV_TF_HEIGHT   | takeoff height                                               |           | 8.0     |       |
| NAV_ZMAX        |                                                              |           |         |       |
| NAV_ZMAX_DECEND |                                                              |           |         |       |

## NUC

| Name           | Description | Min - Max | Default | Units |
| -------------- | ----------- | --------- | ------- | ----- |
| NUC_ATT_CORR_Z |             |           |         |       |

## Preflight check

| Name      | Description | Min - Max | Default | Units |
| --------- | ----------- | --------- | ------- | ----- |
| PRECHK_EN |             |           | 1       |       |

## RC

| Name             | Description                                                  | Min - Max  | Default | Units |
| ---------------- | ------------------------------------------------------------ | ---------- | ------- | ----- |
| RC_ACRO_TH       | Threshold for selecting acro mode                            | 0.0 - 1.0  | 0.5     |       |
| RC_ALTCTL_TH     | Threshold for selecting altitude control mode                | 0.0 - 1.0  | 0.5     |       |
| RC_ASSIST_TH     | Threshold for selecting assist mode                          | 0.0 - 1.0  | 0.25    |       |
| RC_AUTO_TH       | Threshold for selecting auto mode                            | 0.0 - 1.0  | 0.75    |       |
| RC_DSM_BIND      | DSM binding trigger.<br/>-1 = Idle, 0 = Start DSM2 bind, 1 = Start DSMX bind | -1 - 0 - 1 |         |       |
| RC_FAILS_THR     | Failsafe channel PWM threshold.                              |            | 0       |       |
| RC_HEADREDEF_TH  | Threshold for heading redefinition                           |            | 0.5     |       |
| RC_LOITER_TH     | Threshold for selecting loiter mode                          |            | 0.5     |       |
| RC_MAP_ACRO_SW   | Acro switch channel mapping.                                 | 0 - 18     | 0       |       |
| RC_MAP_ALTCTL_SW | Altitude control switch channel mapping.                     |            | 0       |       |
| RC_MAP_AUX1      | Auxiliary switch 1 channel mapping.                          |            | 0       |       |
| RC_MAP_AUX2      |                                                              |            | 0       |       |
| RC_MAP_AUX3      |                                                              |            | 0       |       |
| RC_MAP_AUX4      |                                                              |            | 0       |       |
| RC_MAP_AUX5      |                                                              |            | 0       |       |
| RC_MAP_FAILSAFE  | Failsafe channel mapping.<br/>The RC mapping index indicates which channel is used for failsafe<br/>If 0, whichever channel is mapped to throttle is used otherwise the value indicates the specific rc channel to use |            | 0       |       |
| RC_MAP_FLAPS     | Flaps channel mapping.                                       |            | 0       |       |
| RC_MAP_HEADREDEF | heading redefinition channel mapping.                        |            |         |       |
| RC_MAP_LOITER_SW | Loiter switch channel mapping.                               |            | 0       |       |
| RC_MAP_MODE_SW   | Mode switch channel mapping.<br/>This is the main flight mode selector.<br/>The channel index (starting from 1 for channel 1) indicates which channel should be used for deciding about the main mode.<br/>A value of zero indicates the switch is not assigned. |            | 0       |       |
| RC_MAP_OFFB_SW   | Offboard switch channel mapping.                             |            | 0       |       |
| RC_MAP_PITCH     | Pitch control channel mapping.<br/>The channel index (starting from 1 for channel 1) indicates which channel should be used for reading pitch inputs from.<br/>A value of zero indicates the switch is not assigned. |            | 2       |       |
| RC_MAP_POSCTL_SW | Posctl switch channel mapping.                               |            | 0       |       |
| RC_MAP_RETURN_SW | Return switch channel mapping.                               |            | 0       |       |
| RC_MAP_ROLL      | Roll control channel mapping.<br/>The channel index (starting from 1 for channel 1) indicates which channel should be used for reading roll inputs from.<br/>A value of zero indicates the switch is not assigned. |            | 1       |       |
| RC_MAP_THROTTLE  | Throttle control channel mapping.<br/>The channel index (starting from 1 for channel 1) indicates which channel should be used for reading throttle inputs from.<br/>A value of zero indicates the switch is not assigned. |            | 3       |       |
| RC_MAP_YAW       | Yaw control channel mapping.<br/>The channel index (starting from 1 for channel 1) indicates which channel should be used for reading yaw inputs from.<br/>A value of zero indicates the switch is not assigned. |            | 4       |       |
| RC_MODE_TYPE     | Transmiter mode type mode1, mode2, mode3 mode4<br/>All settings are based on mode1 mapping***** |            | 1       |       |
| RC_OFFB_TH       | Threshold for selecting offboard mode                        |            | 0.5     |       |
| RC_POSCTL_TH     | Threshold for selecting posctl mode                          |            | 0.5     |       |
| RC_RETURN_TH     | Threshold for selecting return to launch mode                |            | 0.5     |       |

## RC1 - RC14

| Name     | Description                | Min - Max       | Default | Units |
| -------- | -------------------------- | --------------- | ------- | ----- |
| RC1_DZ   | RC Channel 1 dead zone     | 0.0 -  100.0    | 10.0    |       |
| RC1_MAX  | RC Channel 1 maximum value | 1500.0 - 2200.0 |         |       |
| RC1_MIN  | RC Channel 1 minimum value | 800.0 - 1500.0  | 1000.0  |       |
| RC1_REV  | RC Channel 1 Reverse       | -1.0 - 1.0      | 1.0     |       |
| RC1_TRIM | RC Channel 1 Trim          | 800.0 - 2200.0  | 1500.0  |       |

## SDLOG

| Name        | Description                                                  | Min - Max  | Default | Units |
| ----------- | ------------------------------------------------------------ | ---------- | ------- | ----- |
| SDLOG_EXT   | Enable extended logging mode.<br/>A value of -1 indicates the commandline argument should be obeyed. A value of 0 disables extended logging mode, a value of 1 enables it. This parameter is only read out before logging starts (which commonly is before arming). | -1 - 0 - 1 | -1      |       |
| SDLOG_INDEX |                                                              |            |         |       |
| SDLOG_RATE  | Logging rate.<br/>A value of -1 indicates the commandline argument should be obeyed. A value of 0 sets the minimum rate any other value is interpreted as rate in Hertz. Thisparameter is only read out before logging starts (which commonly is before arming). | -1 - 0 - 1 | -1      |       |

## System

| Name             | Description                                                  | Min - Max | Default | Units |
| ---------------- | ------------------------------------------------------------ | --------- | ------- | ----- |
| SYS_AUTOCONFIG   | Automatically configure default values.<br/>Set to 1 to set platform-specific parameters to their default values on next system startup. | 0 - 1     | 0       |       |
| SYS_AUTOSTART    | Auto-start script index.<br/>Defines the auto-start script used to bootstrap the system. |           | 0       |       |
| SYS_RESTART_TYPE | Set restart type<br/>Set by px4io to indicate type of restart | 0 - 2     | 2       |       |
| SYS_USE_IO       | Set usage of IO board<br/>Can be used to use a standard startup script but with a FMU only set-up. Set to 0 to force the FMU only set-up. | 0 - 1     | 1       |       |

## Test

NOT used.

| Name       | Description | Min - Max | Default | Units |
| ---------- | ----------- | --------- | ------- | ----- |
| TEST_D     |             |           |         |       |
| TEST_DEV   |             |           |         |       |
| TEST_D_LP  |             |           |         |       |
| TEST_HP    |             |           |         |       |
| TEST_I     |             |           |         |       |
| TEST_I_MAX |             |           |         |       |
| TEST_LP    |             |           |         |       |
| TEST_MAX   |             |           |         |       |
| TEST_MEAN  |             |           |         |       |
| TEST_MIN   |             |           |         |       |
| TEST_P     |             |           |         |       |
| TEST_TRIM  |             |           |         |       |

## Temperature compensation

NOT used.

| Name           | Description                     | Min - Max | Default | Units |
| -------------- | ------------------------------- | --------- | ------- | ----- |
| TMP_CMP_ACX_K1 | Temperature compensation factor |           |         |       |
| TMP_CMP_ACX_K2 |                                 |           |         |       |
| TMP_CMP_ACY_K1 |                                 |           |         |       |
| TMP_CMP_ACY_K2 |                                 |           |         |       |
| TMP_CMP_ACZ_K1 |                                 |           |         |       |
| TMP_CMP_ACZ_K2 |                                 |           |         |       |
| TMP_CMP_GYX_K1 |                                 |           |         |       |
| TMP_CMP_GYX_K2 |                                 |           |         |       |
| TMP_CMP_GYY_K1 |                                 |           |         |       |
| TMP_CMP_GYY_K2 |                                 |           |         |       |
| TMP_CMP_GYZ_K1 |                                 |           |         |       |
| TMP_CMP_GYZ_K2 |                                 |           |         |       |

## Trim

| Name       | Description | Min - Max | Default | Units |
| ---------- | ----------- | --------- | ------- | ----- |
| TRIM_PITCH |             |           | 0.0     |       |
| TRIM_ROLL  |             |           | 0.0     |       |
| TRIM_YAW   |             |           | 0.0     |       |

## UAV

| Name          | Description                                                  | Min - Max | Default     | Units |
| ------------- | ------------------------------------------------------------ | --------- | ----------- | ----- |
| UAV_CM        | motor coefficient                                            |           | 440.9687    |       |
| UAV_CM_BAT_EN |                                                              |           | 0           |       |
| UAV_CM_BAT_K1 |                                                              |           | 9.5149      |       |
| UAV_CM_BAT_K2 |                                                              |           | 55.9678     |       |
| UAV_CM_MAX    |                                                              |           | 560.0       |       |
| UAV_CM_MIN    |                                                              |           | 440.0       |       |
| UAV_CT        | motor coefficient                                            |           | 37.0633f    |       |
| UAV_IX        | vehicle x-axis inertia                                       |           | 0.311       |       |
| UAV_IY        | vehicle y-axis inertia                                       |           | 0.311       |       |
| UAV_IZ        | vehicle z-axis inertia                                       |           | 0.529       |       |
| UAV_KQ1       | motor torque coefficient                                     |           | 0.000012087 |       |
| UAV_KQ2       | motor torque coefficient                                     |           | 0.000013324 |       |
| UAV_KT1       | motor thrust coefficient                                     |           | 0.00039915  |       |
| UAV_KT2       | motor thrust coefficient                                     |           | 0.00044879  |       |
| UAV_LM        | vehicle arm length(from motor to vehicle center)             |           | 0.5         |       |
| UAV_MASS      | vehicle mass                                                 |           | 7.6         |       |
| UAV_MOTOR_MAX |                                                              |           | 1.0         |       |
| UAV_TEMP      | vehicle temperature                                          |           | 25.0        |       |
| UAV_TYPE      | 0 for quad, 1 for hex, 2 for oct x8 configuration, 3 for normal x8 configuration | 0 - 3     | 0           |       |

# Default parameter configuration

For MH06-3 vehicle, the tuned-well parameter has been saved as a file [here](Z:\飞控软件组\WIKI). You could upload it to finish parameter configuration instead of setuping every one step by step.