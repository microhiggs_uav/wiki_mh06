# Introduction

MH06 is an advanced technically fishing-industry-facing quadcopter developed by MicroHiggs Ltd. It consists of airframe, power system, flight control autopilot, RemoteControl(RC), GroundControlStation(GCS) APP and payload(typically camera and/or finishing sonar).



![](./image/introduction/1.jpg)



- [Airframe](./Airframe)
- [Power System](./Power%20System)
- [Autopilot User Guide](./Autopilot%20User%20Guide)
- [Autopilot Develop Guide](./Autopilot%20Develop%20Guide)
- [Parameter Configuration](./Parameter%20Reference%20and%20Configuration)
- [Remote Control Manual](./Remote%20Control%20Manual)
- [Ground Control Station](./Ground%20Control%20Station)
- [Payload](./Payload)

## Key features:

- IP67 waterproof quadcopter
- Able to land on the sea to perform tasks
- Detecting fish mode brings the capability to automatcally plan a mission and finish detecting finshes in the specified sea zone
- 3-in-1 communication link enables real-time vedioes and sonar-scan acoustic images
- 3 flight modes satisfy operating demands for almost all scenarios